package main.kotlin


class Parser(private val tokens: Lexer) {

    fun parseExpression() = parseOperatorExpression(0)

    private fun parseOperatorExpression(minBindingPower: Int): NamedExpr {
        var leftHandSide = parseApplication()
        loop@ while (true) {
            val operator = when (val op = tokens.peek()) {
                is Token.PLUS -> "+"
                else -> break@loop
            }
            val fn = functionForOperator(operator)
            val (leftBP, rightBP) = bindingPowerForOperator(operator)
            if (leftBP < minBindingPower) break
            expectNext<Token.PLUS>("operator")
            val rightHandSide = parseOperatorExpression(rightBP)
            leftHandSide = NamedExpr.Plus(leftHandSide, rightHandSide)
        }
        return leftHandSide
    }

    private fun functionForOperator(operator: String): NamedExpr = when (operator) {
        "+" -> NamedExpr.Var("add")
        "-" -> NamedExpr.Var("subtract")
        "*" -> NamedExpr.Var("multiply")
        "==" -> NamedExpr.Var("equals")
        else -> throw Exception("Unknown operator $operator")
    }

    private fun bindingPowerForOperator(operator: String): Pair<Int, Int> = when (operator) {
        "==" -> 1 to 2
        "+", "-" -> 2 to 3
        "*" -> 3 to 4
        else -> throw Exception("Unknown operator $operator")
    }

    private fun parseApplication(): NamedExpr {
        val atoms = mutableListOf<NamedExpr>()
        while (true) {
            val atom = parseAtom() ?: break
            atoms += atom
        }

        return when {
            atoms.isEmpty() -> throw Exception("Unexpected ${tokens.peek()} expected expression")
            else -> atoms.drop(1).fold(atoms[0]) { acc, expr ->
                NamedExpr.Application(acc, expr)
            }
        }
    }

    private fun parseAtom(): NamedExpr? = when (tokens.peek()) {
        is Token.NUMBER -> number()
        is Token.IDENT -> ident()
        is Token.LEFT_PAREN -> parenthesized()
        is Token.LAMBDA -> lambda()
        else -> null
    }

    private fun lambda(): NamedExpr.Lambda {
        expectNext<Token.LAMBDA>("lambda")
        val binder = expectNext<Token.IDENT>("binder").ident
        expectNext<Token.RIGHT_ARROW>("right arrow")
        val body = parseExpression()
        return NamedExpr.Lambda(binder, body)
    }

    private fun parenthesized(): NamedExpr {
        expectNext<Token.LEFT_PAREN>("opening paren")
        val expr = parseExpression()
        expectNext<Token.RIGHT_PAREN>("closing paren")
        return expr
    }

    private fun ident(): NamedExpr.Var {
        val ident = expectNext<Token.IDENT>("ident").ident
        return NamedExpr.Var(ident)
    }


    private fun number(): NamedExpr.Number {
        val number = expectNext<Token.NUMBER>("number").number
        return NamedExpr.Number(number)
    }

    private inline fun <reified T> expectNext(msg: String): T {
        val next = tokens.next()
        if (next is T) {
            return next
        } else {
            throw Exception("Expected $msg, but saw $next")
        }
    }
}

fun main() {
    val input = """
        \f -> (\x -> f \v -> x x v) (\x -> f \v -> x x v)
    """.trimIndent()

    val lexer = Lexer(input)
    val parser = Parser(lexer)

    val expr = parser.parseExpression()
    println("Parsed\n=======")
    println(expr)
    println()
    println("Evaled\n=======")
    println(correctEval(expr))

    fun parseExpr(s: String) = Parser(Lexer(s)).parseExpression()

    val z = parseExpr("""\f -> (\x -> f \v -> x x v) (\x -> f \v -> x x v)""")
    val faculty = parseExpr(
            """
        \fac -> \x ->
            if x == 0
            then 1
            else x * fac (x - 1)
    """.trimIndent()
    )

    println(correctEval(NamedExpr.Application(NamedExpr.Application(z, faculty), NamedExpr.Number(5))))
}