package main.kotlin

sealed class NamelessExpr {
    data class Plus(val summand1: NamelessExpr, val summand2: NamelessExpr) : NamelessExpr()
    data class Number(val number: Int) : NamelessExpr()
    data class FreeVar(val name: String) : NamelessExpr()
    data class BoundVar(val index: Int) : NamelessExpr()
    data class Lambda(val body: NamelessExpr) : NamelessExpr()
    data class Application(val func: NamelessExpr, val argument: NamelessExpr) : NamelessExpr()
}

sealed class NamedExpr {
    data class Plus(val summand1: NamedExpr, val summand2: NamedExpr) : NamedExpr()
    data class Number(val number: Int) : NamedExpr()
    data class Var(val name: String) : NamedExpr()
    data class Lambda(val binder: String, val body: NamedExpr) : NamedExpr()
    data class Application(val func: NamedExpr, val argument: NamedExpr) : NamedExpr()
}

var i = 0
fun getFreshVariable() = "x" + i++


fun incorrectEval(expr: NamedExpr): NamedExpr {
    return when (expr) {
        is NamedExpr.Plus -> {
            val summand1 = incorrectEval(expr.summand1)
            val summand2 = incorrectEval(expr.summand2)
            if (summand1 is NamedExpr.Number && summand2 is NamedExpr.Number) {
                NamedExpr.Number(summand1.number + summand2.number)
            } else {
                throw Exception("Can't add $summand1 to $summand2")
            }
        }
        is NamedExpr.Number -> expr
        is NamedExpr.Var -> expr
        is NamedExpr.Lambda -> expr
        is NamedExpr.Application -> {
            when (val lambda = incorrectEval(expr.func)) {
                is NamedExpr.Lambda -> incorrectEval(substitute(lambda.binder, incorrectEval(expr.argument), lambda.body))
                else -> throw Exception("${lambda} is not a function")
            }
        }
    }
}

// Replaces all occurences of `binder` with `replacement`
// [binder -> replacement] expr
fun substitute(binder: String, replacement: NamedExpr, expr: NamedExpr): NamedExpr {
    return when (expr) {
        is NamedExpr.Plus -> NamedExpr.Plus(
                substitute(binder, replacement, expr.summand1),
                substitute(binder, replacement, expr.summand2)
        )
        is NamedExpr.Number -> expr
        is NamedExpr.Var -> if (expr.name == binder) {
            replacement
        } else {
            expr
        }
        is NamedExpr.Lambda -> if (expr.binder == binder) {
            expr
        } else {
            NamedExpr.Lambda(expr.binder, substitute(binder, replacement, expr.body))
        }
        is NamedExpr.Application -> NamedExpr.Application(
                substitute(binder, replacement, expr.func),
                substitute(binder, replacement, expr.argument)
        )
    }
}


fun correctEval(expr: NamedExpr) = toNamed(eval(fromNamed(expr, arrayListOf())), arrayListOf())

fun eval(expr: NamelessExpr): NamelessExpr {
    return when (expr) {
        is NamelessExpr.Plus -> {
            val summand1 = eval(expr.summand1)
            val summand2 = eval(expr.summand2)
            if (summand1 is NamelessExpr.Number && summand2 is NamelessExpr.Number) {
                NamelessExpr.Number(summand1.number + summand2.number)
            } else {
                expr
            }
        }
        is NamelessExpr.Number -> expr
        is NamelessExpr.FreeVar -> expr
        is NamelessExpr.BoundVar -> expr
        is NamelessExpr.Lambda -> expr
        is NamelessExpr.Application -> {
            when (val lambda = eval(expr.func)) {
                is NamelessExpr.Lambda -> {
                    return eval(termOpening(lambda.body, expr.argument, 0))
                }
                else -> throw Exception("${lambda} is not a function")
            }
        }
    }
}

fun termOpening(expr: NamelessExpr, subsitute: NamelessExpr, k: Int): NamelessExpr {
    return when (expr) {
        is NamelessExpr.Plus -> NamelessExpr.Plus(termOpening(expr.summand1, subsitute, k),
                                                  termOpening(expr.summand2, subsitute, k))
        is NamelessExpr.Number -> expr
        is NamelessExpr.FreeVar -> expr
        is NamelessExpr.BoundVar -> if (expr.index == k) subsitute else expr
        is NamelessExpr.Lambda -> NamelessExpr.Lambda(termOpening(expr.body, subsitute, k + 1))
        is NamelessExpr.Application -> NamelessExpr.Application(termOpening(expr.func, subsitute, k),
                                                                termOpening(expr.argument, subsitute, k))
    }
}


fun toNamed(expr: NamelessExpr, boundVars: ArrayList<Pair<String, Int>>): NamedExpr {
    return when(expr) {
        is NamelessExpr.Plus -> NamedExpr.Plus(toNamed(expr.summand1, boundVars), toNamed(expr.summand2, boundVars))
        is NamelessExpr.Number -> NamedExpr.Number(expr.number)
        is NamelessExpr.FreeVar -> NamedExpr.Var(expr.name)
        is NamelessExpr.BoundVar -> {
            boundVars.forEach{
                if (it.second == expr.index) return NamedExpr.Var(it.first)
            }
            throw Error("BoundVar index doesn't match any Lambdas")
        }
        is NamelessExpr.Lambda -> {
            val freshVar = getFreshVariable()
            val boundvar = Pair(freshVar, -1)
            boundVars.add(boundvar)
            val namedExpr = NamedExpr.Lambda(freshVar, toNamed(expr.body, boundVars.map{it.first to it.second +1} as ArrayList<Pair<String, Int>>))
            boundVars.remove(boundvar)
            return namedExpr
        }
        is NamelessExpr.Application -> return NamedExpr.Application(toNamed(expr.func, boundVars), toNamed(expr.argument, boundVars))
    }
}

fun fromNamed(expr: NamedExpr, boundVars: ArrayList<Pair<String, Int>>): NamelessExpr {
    when(expr) {
        is NamedExpr.Plus -> return NamelessExpr.Plus(fromNamed(expr.summand1, boundVars), fromNamed(expr.summand2, boundVars))
        is NamedExpr.Number -> return NamelessExpr.Number(expr.number)
        is NamedExpr.Var ->
            if (boundVars.size != 0)
                boundVars.forEach{
                    if (expr.name == it.first) return NamelessExpr.BoundVar(it.second)
                }
            else return NamelessExpr.FreeVar(expr.name)

        is NamedExpr.Lambda -> {
            val pair = Pair(expr.binder, -1)
            boundVars.add(pair)
            val namelessExpr = NamelessExpr.Lambda(fromNamed(expr.body, boundVars.map{it.first to it.second + 1} as ArrayList<Pair<String, Int>>))
            boundVars.remove(pair)
            return namelessExpr
        }
        is NamedExpr.Application -> {
            return NamelessExpr.Application(fromNamed(expr.func, boundVars), fromNamed(expr.argument, boundVars))
        }
    }
    throw Error("wtf")
}


fun parseExpr(s: String) = Parser(Lexer(s)).parseExpression()

fun showEvalDiff(expr: String) {
    val parsedExpr = parseExpr(expr)
    println("Evaluating: $expr")
    println("Korrekt: " + correctEval(parsedExpr))
    println("Inkorrekt:" + incorrectEval(parsedExpr))
}

fun main() {
    val rechnen = """(\x -> (\y -> (x + y))) y"""
    showEvalDiff(rechnen)

    println()

    val input = """(\x -> (\y -> (x + y))) y 3"""
    showEvalDiff(input)
}
